# aboutPage.py
# -*- coding: utf-8 -*-
"""
Created on
@Time: 2024/12/23 16:36
@Author: Mr.Z
@contact: <QQ:40061980>
@contact: bob@pheks.com
@File: aboutPage.py
@IDE: PyCharm
@Python Version：
"""
from PySide6.QtGui import Qt, QPixmap, QFont
from PySide6.QtWidgets import QWidget, QVBoxLayout, QLabel, QPushButton, QHBoxLayout, QMessageBox


class AboutPage(QWidget):
    """关于页面类"""

    def __init__(self):
        """初始化函数"""
        super().__init__()  # 调用父类QWidget的初始化函数

        # 创建垂直布局
        layout = QVBoxLayout()

        # 创建标签，显示关于信息
        about_label = QLabel("软件名称: 我的应用程序\n \n本程序是一个基于PySide6的GUI程序，用于测试PySide6的各种控件的使用。\n \n版本: 1.0.0\n作者: Mr.Z\n联系 "
                             "QQ：40061980\n电子邮箱: bob@pheks.com \n \n版权所有 © 2024 Mr.Z All rights reserved.")
        about_label.setAlignment(Qt.AlignLeft)  # 设置标签左对齐
        about_label.setWordWrap(True)  # 设置标签自动换行
        # 设置标签的字体
        font = about_label.font()
        font.setPointSize(12)
        about_label.setFont(font)

        # 创建一个水平布局用于放置两个二维码及其标题
        qrcode_layout = QHBoxLayout()

        # 创建第一个二维码及其标题的垂直布局
        wechat_layout = QVBoxLayout()
        wechat_title_label = QLabel("开发者微信")
        wechat_title_label.setAlignment(Qt.AlignCenter)  # 设置标题居中对齐
        wechat_font = QFont()
        wechat_font.setPointSize(14)
        wechat_title_label.setFont(wechat_font)

        # 创建第一个二维码图像标签
        wechat_qrcode_label = QLabel()
        wechat_qrcode_pixmap = QPixmap("./static/images/qrcode.png")
        if not wechat_qrcode_pixmap.isNull():
            wechat_qrcode_pixmap = wechat_qrcode_pixmap.scaled(150, 150, Qt.KeepAspectRatio)  # 缩放二维码图片到150x150大小
            wechat_qrcode_label.setPixmap(wechat_qrcode_pixmap)
            wechat_qrcode_label.setAlignment(Qt.AlignCenter)  # 设置二维码居中对齐
        else:
            wechat_qrcode_label.setText("微信二维码加载失败")

        # 将第一个二维码标题和图像添加到第一个垂直布局中
        wechat_layout.addWidget(wechat_title_label)
        wechat_layout.addWidget(wechat_qrcode_label)

        # 创建第二个二维码及其标题的垂直布局
        official_account_layout = QVBoxLayout()
        official_account_title_label = QLabel("关注公众号")
        official_account_title_label.setAlignment(Qt.AlignCenter)  # 设置标题居中对齐
        official_account_font = QFont()
        official_account_font.setPointSize(14)
        official_account_title_label.setFont(official_account_font)

        # 创建第二个二维码图像标签
        official_account_qrcode_label = QLabel()
        official_account_qrcode_pixmap = QPixmap("./static/images/gzh_qrcode.jpg")
        if not official_account_qrcode_pixmap.isNull():
            official_account_qrcode_pixmap = official_account_qrcode_pixmap.scaled(150, 150, Qt.KeepAspectRatio)  # 缩放二维码图片到150x150大小
            official_account_qrcode_label.setPixmap(official_account_qrcode_pixmap)
            official_account_qrcode_label.setAlignment(Qt.AlignCenter)  # 设置二维码居中对齐
        else:
            official_account_qrcode_label.setText("公众号二维码加载失败")

        # 将第二个二维码标题和图像添加到第二个垂直布局中
        official_account_layout.addWidget(official_account_title_label)
        official_account_layout.addWidget(official_account_qrcode_label)

        # 将两个垂直布局添加到水平布局中
        qrcode_layout.addLayout(wechat_layout)
        qrcode_layout.addLayout(official_account_layout)

        # 创建一个按钮，用于显示许可证信息
        license_button = QPushButton("查看许可证")
        license_button.clicked.connect(self.show_license_dialog)  # 连接点击事件到show_license_dialog方法

        # 将标签、二维码标题和二维码图像以及按钮添加到垂直布局中
        layout.addWidget(about_label)
        layout.addLayout(qrcode_layout)
        layout.addWidget(license_button)

        # 设置窗口的布局
        self.setLayout(layout)

    def show_license_dialog(self):
        """显示许可证信息的对话框"""
        try:
            with open("LICENSE.txt", "r", encoding="utf-8") as file:
                license_text = file.read()
            # 创建一个消息框来显示许可证内容
            dialog = QMessageBox()
            dialog.setWindowTitle("许可证信息")
            dialog.setText(license_text)
            dialog.exec()
        except FileNotFoundError:
            QMessageBox.critical(self, "错误", "许可证文件不存在，请检查路径。")
