# -*- coding: utf-8 -*-
"""
Created on
@Time: 2024/12/23 9:32
@Author: Mr.Z
@contact: <QQ:40061980>
@contact: bob@pheks.com
@File: encryptPage.py.py
@IDE: PyCharm
@Python Version：
"""

from datetime import datetime

from PySide6.QtWidgets import QWidget, QVBoxLayout, QLineEdit, QPushButton, QTextEdit, QMessageBox

import utils.encrypt
# 导入utils目录下的encrypt.py文件中的encrypt_password函数
from utils.encrypt import encrypt_password

"""
该组件未使用，仅供参考
"""


class EncryptPage(QWidget):
    def __init__(self, log_text_edit):
        super().__init__()
        self.log_text_edit = log_text_edit

        # 创建主布局
        main_layout = QVBoxLayout()

        # 创建加密密码页面的组件
        self.password_input = QLineEdit()
        self.password_input.setPlaceholderText("请输入密码")
        self.password_input.setEchoMode(QLineEdit.Password)  # 设置为密码输入模式
        main_layout.addWidget(self.password_input)

        self.encrypt_button = QPushButton("加密")
        self.encrypt_button.clicked.connect(self.encrypt_passwd)
        main_layout.addWidget(self.encrypt_button)

        self.encrypted_password_display = QTextEdit()
        self.encrypted_password_display.setReadOnly(True)  # 设置为只读
        self.encrypted_password_display.setPlaceholderText("加密后的密码将显示在这里")
        main_layout.addWidget(self.encrypted_password_display)

        # 设置中心部件的布局
        self.setLayout(main_layout)

    def encrypt_passwd(self):
        password = self.password_input.text()
        key = utils.encrypt.key
        if password:
            # 调用utils目录下的encrypt.py文件中的encrypt_password函数进行加密
            encrypted_password = encrypt_password(password, key)
            # 将加密后的密码显示在encrypted_password_display组件中
            self.encrypted_password_display.setPlainText(encrypted_password)
            # 将加密日志记录到log_text_edit组件中
            self.log_text_edit.appendPlainText(
                f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: 密码已加密，加密结果: {encrypted_password}")
        else:
            QMessageBox.warning(self, "警告", "请输入密码")
            self.log_text_edit.appendPlainText(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: 未输入密码")
