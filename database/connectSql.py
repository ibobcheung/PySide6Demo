# -*- coding: utf-8 -*-
"""
Created on
@Time: 2024/12/24 15:34
@Author: Mr.Z
@contact: <QQ:40061980>
@contact: bob@pheks.com
@File: connectSql.py
@IDE: PyCharm
@Python Version：
"""

import sqlite3
from utils.log_recorder import setup_logger as logger


# 连接数据库

def connect_sql():
    conn = sqlite3.connect('./db/data.db')
    logger().info('[connectSql]-数据库连接成功！')
    cursor = conn.cursor()
    logger().info('[connectSql]游标创建成功！')
    return conn, cursor
