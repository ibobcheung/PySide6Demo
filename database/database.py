# -*- coding: utf-8 -*-
"""
Created on
@Time: 2024/12/24 8:37
@Author: Mr.Z
@contact: <QQ:40061980>
@contact: bob@pheks.com
@File: database.py.py
@IDE: PyCharm
@Python Version：
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker

"""
Database connection is defined here
"""

# 连接本地sqlite数据库
SQLALCHEMY_DATABASE_URL = "sqlite:///./db/data.db"

# 连接postgresql数据库
# SQLALCHEMY_DATABASE_URL = "postgresql://user:password@postgresserver/db"

# 连接mysql数据库
# SQLALCHEMY_DATABASE_URL = 'mysql+pymysql://root:asd123@localhost:3306/inventory'

# 创建数据库引擎
engine = create_engine(SQLALCHEMY_DATABASE_URL)
# 创建会话
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
# 创建基类
Base = declarative_base()


def get_database():
    """
    获取数据库会话
    :return:
    """
    return SessionLocal()  # 返回数据库会话对象


"""
# 数据库连接的另一种方式:使用生成器函数
def get_database():

    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
"""
