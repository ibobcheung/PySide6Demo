# -*- coding: utf-8 -*-
"""
Created on
@Time: 2024/12/24 16:16
@Author: Mr.Z
@contact: <QQ:40061980>
@contact: bob@pheks.com
@File: database_setup.py
@IDE: PyCharm
@Python Version：
"""
import sqlite3
from utils.log_recorder import setup_logger as logger  # 假设你有一个日志记录模块


def create_config_table(cursor):
    """创建配置表"""
    try:
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS Config (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                url TEXT,
                appid TEXT,
                appkey TEXT,
                key_for_appkey TEXT,
                from_email TEXT,
                from_email_password TEXT,
                key_for_email_password TEXT,
                smtp_server TEXT,
                smtp_port TEXT,
                to_email TEXT,
                ftp_host TEXT,
                ftp_user TEXT,
                ftp_password TEXT,
                key_for_ftp_password TEXT,
                ftp_port TEXT,
                api_url TEXT
            )
        ''')
        logger().info("[database_setup-create_config_table]-创建配置表成功")
    except sqlite3.Error as e:
        logger().error(f"[database_setup-create_config_table]-创建配置表时出错: {e}")
        raise e
