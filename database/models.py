# -*- coding: utf-8 -*-
"""
Created on
@Time: 2024/12/24 8:53
@Author: Mr.Z
@contact: <QQ:40061980>
@contact: bob@pheks.com
@File: models.py.py
@IDE: PyCharm
@Python Version：
"""
from sqlalchemy import Integer, Column, String, Sequence

from database.database import Base

"""
Database tables Classes are defined here
"""


# 创建数据库表
class Config(Base):
    __tablename__ = 'config'  # 表名

    id = Column(Integer, Sequence('config_id_seq'), primary_key=True)  # 主键
    url = Column(String)
    appid = Column(String)
    appkey = Column(String)
    key_for_appkey = Column(String)
    from_email = Column(String)
    from_email_password = Column(String)
    key_for_email_password = Column(String)
    smtp_server = Column(String)
    smtp_port = Column(String)
    to_email = Column(String)
    ftp_host = Column(String)
    ftp_user = Column(String)
    ftp_password = Column(String)
    key_for_ftp_password = Column(String)
    ftp_port = Column(String)
    api_url = Column(String)
