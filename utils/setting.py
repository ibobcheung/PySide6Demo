# -*- coding: utf-8 -*-
"""
Created on
@Time: 2024/12/20 11:29
@Author: Mr.Z
@contact: <QQ:40061980>
@contact: bob@pheks.com
@File: setting.py.py
@IDE: PyCharm
@Python Version：
"""

import os

from database.database import get_database
from database.models import Config
from utils.log_recorder import setup_logger as logger

log_path = './logs/'  # 日志保存路径
file_save_path = './download/'  # 下载文件保存路径

# 确保日志路径和下载文件保存路径存在
for path in [log_path, file_save_path]:
    if not os.path.exists(path):
        os.makedirs(path)
        logger().info(f'[setting]-创建路径: {path}')

# 连接数据库
database = get_database()
logger().info('[setting]-数据库连接成功')

# 查询数据库config表中的from_email/from_email_password/key_for_email_password/smtp_server/smtp_port/to_email
config_info = database.query(Config).order_by(Config.id.desc()).first()
logger().info('[setting]-查询数据库配置信息成功')

if config_info:
    from_addr = config_info.from_email
    smtp_password = config_info.from_email_password
    key = config_info.key_for_email_password
    smtp_server = config_info.smtp_server
    smtp_port = config_info.smtp_port
    to_addr = config_info.to_email
    logger().info('[setting]-数据库config表中获取到配置信息')

    # 邮箱配置
    main_info = {
        'from_addr': from_addr,
        'to_addr': to_addr,
        'image_path': '',  # 暂时不附加图片可以保持为空
        'smtp_server': smtp_server,  # 确保这个服务器和端口是正确的
        'smtp_port': smtp_port,  # 确保这里是整数类型
        'key': key,  # 加密密钥，用于解密密码
        'smtp_password': smtp_password  # 确保使用了正确的密码
    }
else:
    logger().warning('[setting]-未从数据库config表中获取到配置信息')
    main_info = {}  # 或者设置为默认值

# 注意：不要在日志中直接输出敏感信息
# logger().info(f'[setting]-邮箱配置: {main_info}')

# 关闭数据库连接
database.close()
logger().info('[setting]-数据库连接关闭成功')

# 接口请求头
api_headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/86.0.4240.198 Safari/537.36',
    'Content-Type': 'application/json;charset=UTF-8',
}

# FTP配置
ftp_info = {
    'ftp_host': '192.168.61.10',
    'ftp_port': 2199,
    'ftp_user': 'EMSGDP',
    'ftp_pass': 'Hyt#147258',
    'ftp_path': '/'
}

mail_text_test = {
    'subject': '测试邮件',
    'message': '这是一封测试邮件，请忽略。'
}

# 邮件模板配置
# mail_template_path = './static/email_template/email_template.html'

mail_text_service_error = {
   'subject': '服务异常通知',
   'message': '抱歉，服务出现异常，请及时查看。'
}
