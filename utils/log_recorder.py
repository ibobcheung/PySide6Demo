# -*- coding: utf-8 -*-
"""
Created on
@Time: 2024/12/20 11:33
@Author: Mr.Z
@contact: <QQ:40061980>
@contact: bob@pheks.com
@File: log_recorder.py
@IDE: PyCharm
@Python Version：
"""

import logging
import os
import time

"""
日志记录器
1. 设置日志记录器
2. 日志文件按日期分割（当前日期加时间戳作为日志文件名）
3. 日志文件过期自动清理（清理30天前的日志文件）
4. 日志目录为./logs，日志文件名格式为 user_log_YYYYMMDD-HHMMSS.log
5. 日志级别为INFO，日志格式为 %(asctime)s - %(levelname)s - %(message)s
"""

log_path = "./logs"  # 日志目录


def cleanup_old_logs(log_directory=log_path, expiration_days=30):
    """清理过期的日志文件"""
    now = time.time()  # 当前时间
    cutoff_time = now - (expiration_days * 86400)  # 30天的秒数

    for filename in os.listdir(log_directory):
        file_path = os.path.join(log_directory, filename)
        # 如果是文件并且上次修改时间超过30天，则删除该文件
        if os.path.isfile(file_path) and os.path.getmtime(file_path) < cutoff_time:
            os.remove(file_path)
            logging.debug(f'删除过期日志文件: {file_path}')  # 输出删除信息


def setup_logger():
    """设置日志记录器"""
    log_directory = log_path  # 日志目录
    # 如果日志目录不存在，则创建日志目录
    os.makedirs(log_directory, exist_ok=True)  # 创建日志目录

    # 使用当前日期加时间戳作为日志文件名
    # current_time = time.strftime("%Y%m%d-%H%M%S")
    # log_file_name = f'user_log_{current_time}.log'

    # 使用当前日期作为日志文件名
    current_date = time.strftime("%Y%m%d")
    log_file_name = f'log_{current_date}.log'

    # 日志文件路径
    log_file_path = os.path.join(log_directory, log_file_name)

    # 设置日志基本配置
    logging.basicConfig(
        level=logging.INFO,  # 日志级别
        format='%(asctime)s - %(levelname)s - %(message)s',  # 日志格式
        handlers=[
            logging.FileHandler(log_file_path, mode='a', encoding='utf-8'),  # 写入日志文件
            logging.StreamHandler()  # 控制台输出
        ]
    )

    cleanup_old_logs(log_directory)  # 在设置完日志后清理过期日志
    return logging


if __name__ == '__main__':
    logger = setup_logger()
    logger.info('This is a test log message.')
