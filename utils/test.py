# -*- coding: utf-8 -*-
"""
Created on
@Time: 2024/12/23 21:45
@Author: Mr.Z
@contact: <QQ:40061980>
@contact: bob@pheks.com
@File: test.py
@IDE: PyCharm
@Python Version：
"""
import sqlite3

from database import database
from database.models import Config
from utils.encrypt import decrypt_password
from database.database import get_database

passwd = "c4eb0723d9e0a7a3541c51cbae937c4262e402468e0b9755f563f91b7b4c01d0"
key = "0b2b6d0bae77d68c741890888a4cf699"

password = bytes.fromhex(passwd)
key2 = bytes.fromhex(key)

original_password = decrypt_password(password, key2)

print(original_password)

conn = sqlite3.connect("../db/data.db")
cursor = conn.cursor()

result = cursor.execute("SELECT from_email_password, key_for_email_password FROM Config").fetchone()
print(f"查询结果：{result}")

config = database.query(Config).first()

a = result[0]
b = result[1]

cursor.close()
conn.close()

print(f"加密密码：{a}, Key：{b}")


