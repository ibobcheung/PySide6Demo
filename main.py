# -*- coding: utf-8 -*-
"""
Created on
@Time: 2024/12/23 8:18
@Author: Mr.Z
@contact: <QQ:40061980>
@contact: bob@pheks.com
@File: main.py
@IDE: PyCharm
@Python Version：
"""

from datetime import datetime

from PySide6.QtCore import QFile
from PySide6.QtGui import QIcon, QFont
from PySide6.QtWidgets import QApplication, QMainWindow, QPushButton, QVBoxLayout, QHBoxLayout, QLabel, \
    QStackedWidget, QMessageBox, QWidget, QPlainTextEdit

from gui.aboutPage import AboutPage  # 导入AboutPage组件
from gui.configPage import ConfigPage  # 导入ConfigPage组件
from gui.infoPage import InfoPage  # 导入InfoPage组件
from gui.excelPage import ExcelPage  # 导入ExcelPage组件
from gui.servicePage import ServicePage  # 导入ServicePage组件


class MainWindow(QMainWindow):
    """主窗口类"""

    def __init__(self):
        """初始化函数"""
        super().__init__()  # 调用父类QMainWindow的初始化函数

        # 设置窗口标题和图标
        self.setWindowTitle("我的应用程序")
        self.setWindowIcon(QIcon("./static/images/logo32x32.png"))  # 设置窗口图标

        # 设置窗口大小
        self.resize(500, 600)

        # 设置窗口的最小大小
        self.setMinimumSize(500, 600)

        # 创建日志文本编辑框
        self.log_text_edit = QPlainTextEdit()
        self.log_text_edit.setReadOnly(True)  # 设置为只读

        # 创建导航栏按钮
        self.log_button = QPushButton("运行日志")
        self.excel_button = QPushButton("Excel工具")
        self.config_button = QPushButton("配置中心")
        self.start_service_button = QPushButton("启动服务")
        self.about_button = QPushButton("关于")  # 添加关于按钮

        # 设置导航栏按钮的点击事件
        self.log_button.clicked.connect(self.show_log_page)
        self.excel_button.clicked.connect(self.show_excel_page)
        self.config_button.clicked.connect(self.show_config_page)
        self.start_service_button.clicked.connect(self.show_start_service_page)
        self.about_button.clicked.connect(self.show_about_page)  # 添加关于按钮的点击事件

        # 创建堆叠窗口部件，用于存放不同的页面
        self.stacked_widget = QStackedWidget()

        # 创建日志页面（InfoPage）
        self.info_page = InfoPage(self.log_text_edit)
        self.stacked_widget.addWidget(self.info_page)

        # 创建Excel工具页面
        self.excel_page = ExcelPage(self.log_text_edit)
        self.stacked_widget.addWidget(self.excel_page)

        # 创建配置页面并传递日志文本编辑框
        self.config_page = ConfigPage(self.log_text_edit)
        self.stacked_widget.addWidget(self.config_page)

        # 创建启动服务页面
        self.start_service_page = ServicePage(self.log_text_edit)
        self.stacked_widget.addWidget(self.start_service_page)

        # 创建关于页面
        self.about_page = AboutPage()
        self.stacked_widget.addWidget(self.about_page)  # 添加关于页面

        # 设置默认显示页面（日志页面）
        self.stacked_widget.setCurrentIndex(0)

        # 高亮默认页面的按钮
        self.highlight_button(self.log_button)

        # 创建主布局
        main_layout = QVBoxLayout()

        # 创建导航栏布局
        nav_layout = QHBoxLayout()

        # 将导航栏按钮添加到导航栏布局中
        nav_layout.addWidget(self.log_button)
        nav_layout.addWidget(self.excel_button)
        nav_layout.addWidget(self.config_button)
        nav_layout.addWidget(self.start_service_button)
        nav_layout.addWidget(self.about_button)  # 添加关于按钮到导航栏布局

        # 将导航栏布局添加到主布局
        main_layout.addLayout(nav_layout)

        # 将堆叠窗口部件添加到主布局
        main_layout.addWidget(self.stacked_widget)

        # 设置中心部件
        container = QWidget()
        container.setLayout(main_layout)
        self.setCentralWidget(container)

    def highlight_button(self, button):
        # 重置所有按钮的样式
        self.log_button.setStyleSheet("")
        self.excel_button.setStyleSheet("")
        self.config_button.setStyleSheet("")
        self.start_service_button.setStyleSheet("")
        self.about_button.setStyleSheet("")  # 重置关于按钮的样式

        # 高亮当前按钮的样式
        button.setStyleSheet("background-color: #1e90ff; color: white;")

    def show_log_page(self):
        # 设置堆叠窗口部件的当前索引为0，显示日志页面
        self.stacked_widget.setCurrentIndex(0)
        self.info_page.load_log()  # 加载日志文件
        self.log_text_edit.appendPlainText(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: 日志页面已显示")
        self.highlight_button(self.log_button)

    def show_excel_page(self):
        # 设置堆叠窗口部件的当前索引为1，显示Excel工具页面
        self.stacked_widget.setCurrentIndex(1)
        self.log_text_edit.appendPlainText(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: Excel工具页面已显示")
        self.highlight_button(self.excel_button)

    def show_config_page(self):
        # 设置堆叠窗口部件的当前索引为2，显示配置页面
        self.stacked_widget.setCurrentIndex(2)
        self.log_text_edit.appendPlainText(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: 配置页面已显示")
        self.highlight_button(self.config_button)

    def show_start_service_page(self):
        # 设置堆叠窗口部件的当前索引为3，显示启动服务页面
        self.stacked_widget.setCurrentIndex(3)
        self.log_text_edit.appendPlainText(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: 启动服务按钮已点击")
        self.highlight_button(self.start_service_button)

    def show_about_page(self):
        # 设置堆叠窗口部件的当前索引为4，显示关于页面
        self.stacked_widget.setCurrentIndex(4)
        self.log_text_edit.appendPlainText(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: 关于页面已显示")
        self.highlight_button(self.about_button)


if __name__ == "__main__":
    # 创建一个QApplication实例，作为应用程序的入口
    app = QApplication([])

    # 检查图标文件是否存在
    icon_file = QFile("./static/images/logo32x32.png")
    if icon_file.exists():
        # 如果图标文件存在，创建主窗口实例
        window = MainWindow()
        # 显示主窗口
        window.show()
        # 启动应用程序的事件循环
        app.exec()
    else:
        # 如果图标文件不存在，弹出一个错误消息框提示用户检查路径
        QMessageBox.critical(None, "错误", "图标文件不存在，请检查路径。")
