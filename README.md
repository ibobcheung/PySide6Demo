# 使用PySide6开发GUI程序

## 一、PySide6简介

PySide6是Qt for Python的最新版本，是一个跨平台的Python模块，它可以用来开发高性能的GUI程序。PySide6支持Qt5、Qt6、PyQt5、PyQt6、PySide2、PySide3、PySide4、PySide5等Python绑定。

PySide6的许可证是GPLv3，可以***免费用于商业***用途。

## 二、PySide6安装

PySide6的安装非常简单，只需要在命令行中运行以下命令：

```
pip install PySide6
```

## 三、项目结构

PySide6的项目结构如下：

```
.
├── db                  # 数据库文件目录
├── gui                 # 程序的GUI界面目录
│   ├── aboutPage.py      # 关于页面
│   ├── configPage.py     # 配置页面
│   ├── excelPage.py      # Excel工具页面
│   ├── infoPage.py       # 日志信息页面
│   └── servicePage.py    # 启动后端服务页面
├── logs                # 日志文件目录, 日志信息页面会读取该目录下面的日志文件
├── database            # SQLAlchemy数据库模型目录
├── static              # 静态资源目录
├── utils               # 工具类目录
├── main.py             # 程序的主入口文件
├── requirements.txt    # 项目依赖文件
├── LICENSE.txt         # 项目许可证文件, 关于页面查看许可证会显示该文件的内容
├── LICENSE_DETAIL.txt  # 项目许可证详细说明文件
└── README.md           # 项目说明文件
```

## 四、运行

### 4.1 创建虚拟环境

```
python -m venv .venv
```
### 4.2 激活虚拟环境

```
# Linux/Mac
source .venv/bin/activate

# Windows
.venv\Scripts\activate.bat
```

### 4.3 安装依赖

```
pip install -r requirements.txt
```

### 4.4 运行程序

```
python main.py
```


## 五、使用PyInstaller打包程序

### 6.1 安装PyInstaller

```
pip install pyinstaller
```

### 6.2 运行PyInstaller命令

```
pyinstaller -w --add-data "static:static" -i E://Workspace/project/PySide6Demo/static/images/logo.ico -n ExcelTools --version-file version.txt main.py
```

> 注意：
> 1. 打包前先激活虚拟环境并安装依赖，在虚拟环境下进行打包。
> 2. 在虚拟环境下安装PyInstaller。
> 3. 项目目录下有version.txt文件，该文件用于指定程序的版本号。
> 4. 项目目录下有static文件夹，该文件夹用于存放静态资源。
> 5. 打包完成后，将项目中的db、static文件夹复制到exe所在目录，并确保和exe同级目录。
> 6. 参数说明：
>    -w：表示不显示命令行窗口。
>    --add-data：表示添加数据文件。
>    -i：表示设置图标。
>    -n：表示设置程序名称。
>    --version-file：表示设置版本号。

### 6.3 运行程序

```
dist\main.exe
```

## 七、软件截图

![img.png](./static/README/img.png)
![img_1.png](./static/README/img_1.png)
![img_2.png](./static/README/img_2.png)
![img_3.png](./static/README/img_3.png)
![img_4.png](./static/README/img_4.png)